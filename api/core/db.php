<?php

/**
 * Created by PhpStorm.
 * User: VBO
 * Date: 02.07.2017
 * Time: 11:01
 */
class DB {
    public $mysqli;

    function __construct() {
    	$this->mysqli = new mysqli('localhost','root', '', 'rest');
    }

    public function query( $query_str = '' ) {
    	if(!empty($query_str) && is_string($query_str)){
        	$result = $this->mysqli->query($query_str);
        }

        if(!empty($result)){
        	return $result;
        } else {
        	return 'Соединение с базой данных не удалось или ошибка в отправленном запросе.';
        }
    }
}