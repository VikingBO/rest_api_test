<?php

/**
 * Created by PhpStorm.
 * User: VBO
 * Date: 04.07.2017
 * Time: 3:47
 */
class response {
	public $db;
	static $response = array( 'status' => 'error', 'data'=>array(), 'message' => '');

    public function __construct() {
        if(class_exists('DB')){
            $this->db = new DB();
        } else {
        	self::$response['message'] = 'Нет подключения к классу базы данных, обратитесь к разработчикам.';
        	return self::$response;
        }
    }

	public function Table( $table_name = '', $id = 0 ) {
    	if($this->db->mysqli->connect_errno) {
    		self::$response['message'] = "Не удалось подключиться: %s\n".$this->db->mysqli->connect_error;
			return self::$response;
		} else {
			if(!empty($table_name)){
				$table_name = trim($table_name);
				$table_name = $this->db->mysqli->real_escape_string( $table_name);


				if(!empty($id) && is_integer($id)){
					if($this->db->mysqli->real_query('SELECT DISTINCT * FROM '.$table_name.' WHERE `ID`='.$id)){
						foreach ($this->db->mysqli->use_result() as $test) {
							self::$response['data'][] = $test;
						}
					}
				} else {
					if($this->db->mysqli->real_query('SELECT * FROM '.$table_name)){
						foreach ($this->db->mysqli->use_result() as $test) {
							self::$response['data'][] = $test;
						}
					}
					if(!empty($id) && !is_integer($id) || $id == 0){
						self::$response['message'] = 'Второй параметр не является числом поэтому не может быть использован';
					} else {
						self::$response['message'] = 'Данные о таблице получены успешно.';
					}
				}

				self::$response['status'] = 'ok';

				return self::$response;
			} else {
				self::$response['status'] = 'ok';
				self::$response['data'] = array();
				self::$response['message'] = 'Вы не указали какую же таблицу необходимо получить';

				return self::$response;
			}
		}
    }

	public function SessionSubscribe( $sessionId = 0, $userId = 0 ) {
		if($this->db->mysqli->connect_errno) {
			self::$response['message'] = "Не удалось подключиться: %s\n".$this->db->mysqli->connect_error;
			return self::$response;
		} else {

		}
    }
}