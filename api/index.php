<?php
/**
 * Created by PhpStorm.
 * User: VBO
 * Date: 02.07.2017
 * Time: 11:01
 */

require_once 'core/db.php';
require_once 'core/response.php';

$response = new response();

if(method_exists($response, $uri_arr[1])){
	$method = $uri_arr[1];

	if($method == 'Table' && !empty($uri_arr[3])){
		$response_arr = $response->$method($uri_arr[2], (int) $uri_arr[3]);
	} else {
		$response_arr = $response->$method($uri_arr[2]);
	}

	$json_response = json_encode($response_arr, JSON_UNESCAPED_UNICODE);
	printf($json_response);
}