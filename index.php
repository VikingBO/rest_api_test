<?php
/**
 * Created by PhpStorm.
 * User: VBO
 * Date: 04.07.2017
 * Time: 3:13
 */

ini_set('display_errors', 'On');
error_reporting(E_ALL);

if(!empty($_SERVER['REQUEST_URI'])){
	$uri = substr($_SERVER['REQUEST_URI'], 1);
	$uri_arr = explode('/', $uri);
	if($uri_arr[0] == 'api'){
		require_once('api/index.php');
	}
} else {
	echo 'Hello world, я рад вас видеть но это не REST API запрос :)';
}

